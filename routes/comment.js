var express = require('express');
var router = express.Router();

const bodyParser = require('body-parser');
var commentSchema = require('./../my_modules/my_db_module/MyCommentSchema');
const mongoose = require('mongoose');
const myDBConfig = require('./../my_modules/my_db_module/MyDBConfig')
var myDBTools = require('./../my_modules/my_db_module/MyDBTools')

var myResult = require('./../my_modules/MyResult')
var result = myResult.getInstance()

const myDB = myDBTools.getInstance()

var comment = mongoose.model(myDBConfig.comment, commentSchema, myDBConfig.comment);

router.get('/', function(req, res, next) {
    res.send('respond with a resource');
  });

  router.post('/add', function(req, res, next) {
    console.log(req.body)
  
   var d = new Date().getTime();
  
  var c = new comment({ commenterUUID:req.body.commenterUUID, uuid:req.body.uuid, commenterImage:req.body.commenterImage, comment:req.body.comment,
      date:d, dateStr:req.body.dateStr, commenterName:req.body.commenterName, blogUUID: req.body.blogUUID })
  
    myDB.mySaveToDB(c, "comment add ok", "comment add error", function(err, response){
        if (err != null){
            return res.send(result.myErrorResponseJson(err.message))
        }else{
          return res.send(result.mySuccessfulResponseJson('successful'))
        }
    })
  });

  router.post('/getComment', function (req, res, next) {
    console.log('comment/getComment \n')
    console.log(req.body)

    myDB.myFindDBWithSortAndLimit(comment, {'blogUUID': {$in: req.body.blogUUID}},
      100, "comment/getComment ok ", "comment/getComment error ",
       function(err, doc){
         if (err) {
            return res.send(result.myErrorResponseJson(err.message))
         }else{
            return res.send(result.mySuccessfulResponseJson(doc))
         }
     })
  })

  module.exports = router;