var express = require('express');
var router = express.Router();

const bodyParser = require('body-parser');
var blogSchema = require('./../my_modules/my_db_module/MyBlogSchema');
const mongoose = require('mongoose');
const myDBConfig = require('./../my_modules/my_db_module/MyDBConfig')
var myDBTools = require('./../my_modules/my_db_module/MyDBTools')

var myResult = require('./../my_modules/MyResult')
var result = myResult.getInstance()

const myDB = myDBTools.getInstance()

var blog = mongoose.model(myDBConfig.blog, blogSchema, myDBConfig.blog);

router.get('/', function(req, res, next) {
    res.send('respond with a resource');
  });

  router.post('/add', function(req, res, next) {
    console.log(req.body)
  
   var d = new Date().getTime();
  
  var b = new blog({ authorUUID:req.body.authorUUID, uuid:req.body.uuid, image:req.body.image, authorImage:req.body.authorImage,
      content:req.body.content, date:d, dateStr:req.body.dateStr, privacy:req.body.privacy,
      type:req.body.type, authorName:req.body.authorName, authorAge:req.body.authorAge,
      authorHobby:req.body.authorHobby, authorGender:req.body.authorGender })
  
    myDB.mySaveToDB(b, "blog add ok", "blog add error", function(err, response){
        if (err != null){
            return res.send(result.myErrorResponseJson(err.message))
        }else{
          return res.send(result.mySuccessfulResponseJson('successful'))
        }
    })
  });

  router.post('/getBlogFromHobby', function (req, res, next) {
    console.log('blogs/getBlogFromHobby \n')
    console.log(req.body)

    myDB.myFindDBWithSortAndLimit(blog, {'authorUUID': {$in: req.body.friends}},
      10, "blogs/getBlogFromHobby ok ", "blogs/getBlogFromHobby error ",
       function(err, doc){
         if (err) {
            return res.send(result.myErrorResponseJson(err.message))
         }else{
            return res.send(result.mySuccessfulResponseJson(doc))
         }
     })
  })

  router.post('/blogFromThisUser', function (req, res, next) {
    console.log('blogs/getBlogFromThisUser \n')
    console.log(req.body) 
    
    myDB.myFindDBWithSortAndLimit(blog, {'authorUUID': {$in: [req.body.authorUUID]}},
      10, "blogs/getBlogFromThisUser ok ", "blogs/getBlogFromThisUser error ",
       function(err, doc){
         if (err) {
            return res.send(result.myErrorResponseJson(err.message))
         }else{
            return res.send(result.mySuccessfulResponseJson(doc))
         }
     })
   })

   router.post('/myBlogs', function (req, res, next) {
    console.log('blogs/myBlogs \n')
    console.log(req.body)
  
    myDB.myFindDBWithSort(blog, {'authorUUID': { $in: [req.body.authorUUID]}},
    "blogs/myBlogs ok", "blogs/myBlogs error", function(err, doc){
        if (err) {
            return res.send(result.myErrorResponseJson(err.message))
          }else{
            return res.send(result.mySuccessfulResponseJson(doc))
          }
    })
  })

  router.post('/deleteBlogs', function (req, res, next) {
    console.log('blogs/deleteBlogs \n')
    console.log(req.body)
  
    myDB.myDeleteDB(blog, {'uuid': { $in: req.body.uuid}},
    "blogs/deleteBlogs ok", "blogs/deleteBlogs error", function(err, msg){
        if (err) {
            return res.send(result.myErrorResponseJson(err.message))
          }else{
            return res.send(result.mySuccessfulResponseJson(msg))
          }
    })
  })

  module.exports = router;