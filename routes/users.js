var express = require('express');
var router = express.Router();

const bodyParser = require('body-parser');
var userSchema = require('./../my_modules/my_db_module/MyUserSchema');
const mongoose = require('mongoose');
const myDBConfig = require('./../my_modules/my_db_module/MyDBConfig')
var myDBTools = require('./../my_modules/my_db_module/MyDBTools')

var myResult = require('./../my_modules/MyResult')
var result = myResult.getInstance()

const myDB = myDBTools.getInstance()

var myDBURL = myDBConfig.dbURL

mongoose.connect(myDBURL,
   { useNewUrlParser: true, useUnifiedTopology: true },
    function (err) {
  if (err) {
    console.log(err);
    return;
  }
  console.log('successfully connect database(' + myDBURL + ')')
});

var user = mongoose.model(myDBConfig.user, userSchema, myDBConfig.user);

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/init', function (req, res, next) {
  console.log('users/init \n')
  console.log(req.body)

  u = new user({ uuid: req.body.uuid, name: req.body.name, age: req.body.age,
    hobby: req.body.hobby, gender: req.body.gender, image: req.body.image,
    introduction: req.body.introduction, friends: new Array() })

  myDB.mySaveToDB(u, "init user ok ", "init user error ", function(err, response){
    if (err != null){
        return res.send(result.myErrorResponseJson(err.message))
    }else{
      return res.send(result.mySuccessfulResponseJson('successful'))
    }
  })
})

router.post('/friendsFromName', function (req, res, next) {
  console.log('users/friendsFromName \n')
  console.log(req.body)

  myDB.myFindDB(user, {'name': {$regex: new RegExp(req.body.name.trim(), "i")}, 
  'uuid': {$nin: [req.body.uuid] }},
   "friendsFromName ok ", "friendsFromName error ", function (err, doc) {
    if (err != null){
      return res.send(result.myErrorResponseJson(err.message))
    }else{
      return res.send(result.mySuccessfulResponseJson(doc))
    }
  })
})

router.post('/recommendedFriends', function (req, res, next) {
  console.log('users/recommendedFriends \n')
  console.log(req.body)

  myDB.myAggregateOne(user, [
    {
      $match:
      {
        'uuid': {
          $nin: [req.body.uuid]
        }
      }
    },
    {
      $match:
      {
        'hobby': {
          $in:
            [req.body.hobby]
        }
      }
    },{
      $match: {
        'uuid': {
          $nin: req.body.friends
        }
      }
    },
    { $sample: { size: 3 } }], "recommendedFriends ok ",
   "recommendedFriends error ",
   function(err, doc){
    if (err){
      return res.send(result.myErrorResponseJson(err.message))
    }else{
      return res.send(result.mySuccessfulResponseJson(doc))
    }
  })
});

router.post('/myFriends', function (req, res, next) {
  console.log('users/myFriends \n')
  console.log(req.body)

  myDB.myAggregateOne(user, [
    {
      $match:
      {
        'uuid': {
          $nin: req.body.uuid
        }
      }
    }
    ,{
      $match: {
        'uuid': {
          $in: req.body.friendsArr
        }
      }
    }],
   "myFriends ok ", "myFriends error ",
   function(err, doc){
    if (err){
      return res.send(result.myErrorResponseJson(err.message))
    }else{
      return res.send(result.mySuccessfulResponseJson(doc))
    }
  })
})

router.post('/fromUUID', function (req, res, next) {
  console.log('users/find \n')
  console.log(req.body)

  myDB.myFindDB(user, { 'uuid': { $in: [req.body.uuid] }}, 
  "fromUUID ok ", "fromUUID error",
   function (err, doc) {
    if (err) {
      return res.send(result.myErrorResponseJson(err.message))
    }
    return res.send(result.mySuccessfulResponseJson(doc))
  })
})

router.post('/editInfo', function (req, res, next) {
  console.log('users/edit \n')
  console.log(req.body)

  myDB.myUpdateOneDB(user, {"uuid": req.body.uuid },
   {"name": req.body.name, "age": req.body.age,
    "hobby": req.body.hobby, "gender": req.body.gender,
    "image": req.body.image, "introduction": req.body.introduction},
     "users/edit ok ",
      "users/edit error", (err, doc) => {
      if (err) {
        return res.send(result.myErrorResponseJson(err.message))
      }
      if (doc.nModified == 0) {
        return res.send(result.mySuccessfulResponseJson('fail'))
      }else{
        return res.send(result.mySuccessfulResponseJson('successful'))
      }
    })
})

router.post('/addFrined', function (req, res, next) {
  console.log('users/addFrined \n')
  console.log(req.body)

  myDB.myUpdateOneDBWithExec(user, {"uuid": req.body.uuid },
  { "$push": { "friends": req.body.friends } },
     "users/addFrined ok ",
     "users/addFrined error", (err, doc) => {
      if (err) {
        return res.send(result.myErrorResponseJson(err.message))
      }
      if (doc.nModified == 0) {
        return res.send(result.mySuccessfulResponseJson('fail'))
      }else{
        return res.send(result.mySuccessfulResponseJson('successful'))
      }
    })
  })
    router.post('/removeFriend', function (req, res, next) {
      console.log('users/removeFriend \n')
      console.log(req.body)

      myDB.myUpdateOneDB(user,{"uuid": req.body.uuid},
      {$pullAll: {"friends":[req.body.friends]}},
       "users/removeFriend ok",
      "users/removeFriend error", (err, doc) => {
      if (err) {
        return res.send(result.myErrorResponseJson(err.message))
      }
      if (doc.nModified) {
        return res.send(result.mySuccessfulResponseJson('fail'))
      }else{
        return res.send(result.mySuccessfulResponseJson('successful' ))
      }
    })
 })

module.exports = router;
