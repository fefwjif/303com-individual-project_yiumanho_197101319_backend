class MyResult{

    static getInstance(){

        if(!MyResult.instance){
            MyResult.instance= new MyResult();
        }
        return  MyResult.instance;
    }

    mySuccessfulResponse(json){
        return json
    }

    mySuccessfulResponseJson (succ){
        var json = JSON.stringify({"value":succ})
        return json
    }

    myErrorResponse (error){
        return error
    }

    myErrorResponseJson (error){
        var json = JSON.stringify({"value":error})
        return json
    }
}

module.exports = MyResult;

