const mongoose = require('mongoose');

var MyBlogSchema = mongoose.Schema({
    authorUUID:String,
    uuid:String,
    image:String,
    authorImage:String,
    content:String,
    date:Number,
    dateStr:String,
    privacy:String,
    type:String,
    authorName:String,
    authorAge:Number,
    authorHobby:String,
    authorGender:String
})

module.exports = MyBlogSchema