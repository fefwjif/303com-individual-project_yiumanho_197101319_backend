const mongoose = require('mongoose');
const myDBConfig = require('./MyDBConfig')

const myDBURL = myDBConfig.dbURL
const myDbname = myDBConfig.dbName

var express = require('express');
var app = express();

mongoose.connect(myDBURL,
    { useNewUrlParser: true, useUnifiedTopology: true },
     function (err) {
   if (err) {
     console.log(err);
     return;
   }
   console.log('successfully connect database(' + myDBURL + ')')
 });

app.use(function (req, res, next) {
    console.log(req.body)
    next();
})

class MyDBTools{

    static getInstance(){

        if(!MyDBTools.instance){
            MyDBTools.instance= new MyDBTools();
        }
        return  MyDBTools.instance;
    }

    mySaveToDB(mySchema, okMsg, errMsg, myCallback){

        mySchema.save(function (err) {
            if (err) {
              console.log(errMsg  + err + '\n')
              myCallback (err, null)
            } else {
                console.log(okMsg + '\n')
                myCallback(null, 'ok save')
          }
        })
      }

    myFindDB(mySchema, mySelection, okMsg, errMsg, myCallback){
      mySchema.find(mySelection, function (err, doc) {
        if (err) {
          console.log(errMsg  + err + '\n')
          myCallback(err, null)
        }else{
          console.log(okMsg + '\n')
          myCallback(null, doc)
        }
      })
    }

    myFindDBWithSortAndLimit(mySchema, mySelection, limitCount, okMsg, errMsg, myCallback){
      mySchema.find(mySelection, function (err, doc) {
        if (err) {
          console.log(errMsg  + err + '\n')
          myCallback(err, null)
        }else{
          console.log(okMsg + '\n')
          myCallback(null, doc)
        }
      }).sort({date: -1}).limit(limitCount)
    }

    myFindDBWithSort(mySchema, mySelection, okMsg, errMsg, myCallback){
      mySchema.find(mySelection, function (err, doc) {
        if (err) {
          console.log(errMsg  + err + '\n')
          myCallback(err, null)
        }else{
          console.log(okMsg + '\n')
          myCallback(null, doc)
        }
      }).sort({date: -1})
    }

    myAggregateOne(mySchema, mySelection, okMsg, errMsg, myCallback){
      mySchema.aggregate(mySelection, function(err, docs){
        if (err) {
          console.log(errMsg + err + ' \n')
          myCallback(err, null)
        }else{
          console.log(okMsg + ' \n')
          myCallback(null, docs)
        }
       })
      }

      myAggregate(mySchema, mySelectionKey, mySelection, okMsg, errMsg, myCallback){
        mySchema.aggregate(mySelectionKey, mySelection, function(err, docs){
          if (err) {
            console.log(errMsg + err + ' \n')
            myCallback(err, null)
          }else{
            console.log(okMsg + ' \n')
            myCallback(null, docs)
          }
         })
        }

    myUpdateOneDB(mySchema, mySelectionKey ,mySelection, okMsg, errMsg, myCallback){
      mySchema.updateOne(mySelectionKey ,mySelection, (err, doc) => {
        if (err) {
          console.log(errMsg + err + ' \n')
          myCallback(err, null)
        }else{
          console.log(okMsg + '\n')
          myCallback(null, doc)
        }
      })
    }

    myUpdateOneDBWithExec(mySchema, mySelectionKey, mySelection, okMsg, errMsg, myCallback){
      mySchema.updateOne(mySelectionKey ,mySelection).exec(function (err, doc){
          if (err) {
            console.log(errMsg + err + ' \n')
            myCallback(err, null)
          }else{
            console.log(okMsg + '\n')
            myCallback(null, doc)
          }
      })
    }

    myDeleteDB(mySchema, mySelection, okMsg, errMsg, myCallback){
      mySchema.remove(mySelection, function(err) {
        if (err) {
          console.log(errMsg + err + ' \n')
          myCallback(err, null)
        }
        else {
          console.log(okMsg + '\n')
          myCallback(null, "successful")
        }
    })
   } 
  }
  
module.exports = MyDBTools;