const mongoose = require('mongoose');

var MyUserSchema=mongoose.Schema({
    uuid:String,
    name:String,
    age:Number,
    hobby: String,
    gender:String,
    image:String,
    introduction:String,
    friends:[{type: String}]
})

module.exports = MyUserSchema