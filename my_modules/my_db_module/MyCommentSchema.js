const mongoose = require('mongoose');

var MyCommentSchema = mongoose.Schema({
    commenterUUID:String,
    uuid:String,
    commenterImage:String,
    comment:String,
    date:Number,
    dateStr:String,
    commenterName:String,
    blogUUID:String
})

module.exports = MyCommentSchema